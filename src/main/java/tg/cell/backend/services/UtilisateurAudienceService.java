package tg.cell.backend.services;

import tg.cell.backend.entities.UtilisateurAudience;

import java.util.List;

public interface UtilisateurAudienceService {
    List<UtilisateurAudience> findAll();

    UtilisateurAudience findById(Long id);

    UtilisateurAudience save(UtilisateurAudience utilisateurAudience);

    UtilisateurAudience update(UtilisateurAudience utilisateurAudience, Long id);
}
