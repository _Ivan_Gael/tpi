package tg.cell.backend.services;


import tg.cell.backend.entities.Statut;

import java.util.List;

public interface StatutService {
    List<Statut> findAll();

    Statut findByCode(String code);

    Statut save(Statut statut);

    Statut update(Statut statut, String code);
}
