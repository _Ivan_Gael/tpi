package tg.cell.backend.services;

import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Document;
import tg.cell.backend.entities.TypeDocument;

import java.util.List;

public interface DocumentService {
    List<Document> findAll();

    Document findBydId(Long id);

    Document save(MultipartFile fichier);

    Document saveDoc(Document document);

    Document update(Document document, Long id);
}
