package tg.cell.backend.services;


import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Personne;
import tg.cell.backend.entities.TypeDocument;
import tg.cell.backend.servicesImpl.FileModel;

import java.util.List;

public interface PersonneService {
    List<Personne> findAll();

    Personne findById(Long id);

    Personne save(Personne personne);

    Boolean saveAllProcess(String demandeur, String typActe, String[] typeDocuments , MultipartFile[] files);

    Personne update(Personne personne, Long id);
}
