package tg.cell.backend.services;

import tg.cell.backend.entities.TypePartie;

import java.util.List;

public interface TypePartieService {
    List<TypePartie> findAll();

    TypePartie findByCode(String code);

    TypePartie save(TypePartie typePartie);

    TypePartie update(TypePartie typePartie, String id);
}
