package tg.cell.backend.services;

import tg.cell.backend.entities.Dossier;

import java.util.List;

public interface DossierService {
    List<Dossier> findAll();

    Dossier findById(Long id);

    Dossier save(Dossier dossier);

    Dossier update(Dossier dossier, Long id);
}
