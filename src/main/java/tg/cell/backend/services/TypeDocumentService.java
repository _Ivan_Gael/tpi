package tg.cell.backend.services;

import tg.cell.backend.entities.TypeDocument;

import java.util.List;

public interface TypeDocumentService {
    List<TypeDocument> findAll();

    TypeDocument findByCode(String code);

    TypeDocument save(TypeDocument typeDocument);

    TypeDocument update(TypeDocument typeDocument, String code);
}
