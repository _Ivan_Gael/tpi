package tg.cell.backend.services;

import tg.cell.backend.entities.Jour;
import tg.cell.backend.entities.Profession;

import java.util.List;

public interface ProfessionService {
    List<Profession> findAll();

    Profession findById(Long id);

    Profession save(Profession profession);

    Profession update(Profession profession, Long id);
}
