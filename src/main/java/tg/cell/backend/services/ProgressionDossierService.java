package tg.cell.backend.services;


import tg.cell.backend.entities.ProgressionDossier;

import java.util.List;

public interface ProgressionDossierService {
    List<ProgressionDossier> findAll();

    ProgressionDossier findByCode(String code);

    ProgressionDossier save(ProgressionDossier progressionDossier);

    ProgressionDossier update(ProgressionDossier progressionDossier, String code);
}
