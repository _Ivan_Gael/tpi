package tg.cell.backend.services;


import tg.cell.backend.entities.NatureAudience;

import java.util.List;

public interface NatureAudienceService {
    List<NatureAudience> findAll();

    NatureAudience findByCode(String code);

    NatureAudience save(NatureAudience natureAudience);

    NatureAudience update(NatureAudience natureAudience, String code);
}
