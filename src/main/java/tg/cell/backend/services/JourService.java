package tg.cell.backend.services;

import tg.cell.backend.entities.Jour;

import java.util.List;

public interface JourService {
    List<Jour> findAll();

    Jour findByCode(String code);

    Jour save(Jour jour);

    Jour update(Jour jour, String code);
}
