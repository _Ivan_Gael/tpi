package tg.cell.backend.services;

import tg.cell.backend.entities.Periode;

import java.util.List;

public interface PeriodeService {
    List<Periode> findAll();

    Periode findByCode(String code);

    Periode save(Periode periode);

    Periode update(Periode periode, String code);
}
