package tg.cell.backend.services;


import tg.cell.backend.entities.TypeActe;
import tg.cell.backend.entities.TypeActeDocument;

import java.util.List;

public interface TypeActeDocumentService {
    List<TypeActeDocument> findAll();

    TypeActeDocument findById(Long id);

    List<TypeActeDocument> findByTypeActe(TypeActe typeActe);

    TypeActeDocument save(TypeActeDocument typeActeDocument);

    TypeActeDocument update(TypeActeDocument typeActeDocument, Long id);
}
