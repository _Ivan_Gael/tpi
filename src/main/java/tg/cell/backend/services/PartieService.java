package tg.cell.backend.services;


import tg.cell.backend.entities.Partie;

import java.util.List;

public interface PartieService {
    List<Partie> findAll();

    Partie findById(Long id);

    Partie save(Partie partie);

    Partie update(Partie partie, Long id);
}
