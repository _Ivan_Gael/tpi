package tg.cell.backend.services;

import tg.cell.backend.entities.Centre;

import java.util.List;

public interface CentreService {
    List<Centre> findAll();
    Centre findById(Long id);
    Centre findByLibelle(String libelle);
    Centre save(Centre centre);
    Centre update(Centre centre,Long id);
}
