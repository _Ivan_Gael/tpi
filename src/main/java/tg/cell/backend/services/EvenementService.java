package tg.cell.backend.services;


import tg.cell.backend.entities.Evenement;

import java.util.List;

public interface EvenementService {
    List<Evenement> findAll();

    Evenement findById(Long id);

    Evenement save(Evenement evenement);

    Evenement update(Evenement evenement, Long id);
}
