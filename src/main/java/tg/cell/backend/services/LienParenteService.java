package tg.cell.backend.services;


import tg.cell.backend.entities.LienParent;

import java.util.List;

public interface LienParenteService {
    List<LienParent> findAll();
    LienParent findById(Long id);
    LienParent save(LienParent lienParent);
    LienParent update(LienParent lienParent,Long id);
}
