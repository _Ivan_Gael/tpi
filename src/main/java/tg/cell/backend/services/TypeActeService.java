package tg.cell.backend.services;

import tg.cell.backend.entities.TypeActe;

import java.util.List;

public interface TypeActeService {
    List<TypeActe> findAll();

    TypeActe findByCode(String code);

    TypeActe save(TypeActe typeActe);

    TypeActe update(TypeActe typeActe, String code);
}
