package tg.cell.backend.services;


import tg.cell.backend.entities.ProgrammationAudience;

import java.util.List;

public interface ProgrammationAudienceService {
    List<ProgrammationAudience> findAll();

    ProgrammationAudience findById(Long id);

    ProgrammationAudience save(ProgrammationAudience programmationAudience);

    ProgrammationAudience update(ProgrammationAudience programmationAudience, Long id);
}
