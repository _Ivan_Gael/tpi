package tg.cell.backend.services;

import tg.cell.backend.entities.Audience;

import java.util.List;

public interface AudienceService {
    List<Audience> findAll();

    Audience findById(Long id);

    Audience save(Audience audience);

    Audience update(Audience audience, Long id);
}
