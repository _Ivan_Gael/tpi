package tg.cell.backend.services;

import tg.cell.backend.entities.Acte;

import java.util.List;

public interface ActeService {
    List<Acte> findAll();

    Acte findById(Long id);

    Acte save(Acte acte);

    Acte update(Acte acte, Long id);
}
