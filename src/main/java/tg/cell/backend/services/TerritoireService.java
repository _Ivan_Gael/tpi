package tg.cell.backend.services;


import tg.cell.backend.entities.Territoire;

import java.util.List;

public interface TerritoireService {
    List<Territoire> findAll();
    Territoire findByCode(String code);
    Territoire save(Territoire territoire);
    Territoire update(Territoire territoire,String code);
}
