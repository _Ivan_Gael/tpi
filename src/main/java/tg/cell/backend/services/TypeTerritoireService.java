package tg.cell.backend.services;

import tg.cell.backend.entities.TypeTerritoire;

import java.util.List;

public interface TypeTerritoireService {
    List<TypeTerritoire> findAll();
    TypeTerritoire findByCode(String code);
    TypeTerritoire save(TypeTerritoire typeTerritoire);
    TypeTerritoire update(TypeTerritoire typeTerritoire,String code);
}
