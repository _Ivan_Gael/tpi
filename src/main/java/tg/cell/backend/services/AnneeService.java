package tg.cell.backend.services;

import tg.cell.backend.entities.Annee;

import java.util.List;

public interface AnneeService {
    List<Annee> findAll();

    Annee findByCode(String code);

    Annee save(Annee annee);

    Annee update(Annee annee, String code);
}
