package tg.cell.backend.services;

import tg.cell.backend.entities.TypePersonne;

import java.util.List;

public interface TypePersonneService {
    List<TypePersonne> findAll();
    TypePersonne findByCode(String code);
    TypePersonne save(TypePersonne typePersonne);
    TypePersonne update(TypePersonne typePersonne, String code);
}
