package tg.cell.backend.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "type_actes")
public class TypeActe implements Serializable {

    @Id
    private String code;

    @Column(nullable = false)
    private String libelle;

    @Column(nullable = false)
    private Long frais;

    @OneToMany(mappedBy = "typeActe", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<TypeActeDocument> typeActeDocuments;
}
