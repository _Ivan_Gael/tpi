package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "documents")
public class Document implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    private String nom;
    
    private String uploadFileName;

    private String repertoire;

    
    private Long taille;

    
    private Date date;

    
    private Date dateDebutImportation;

    
    private Date dateFinImportation;

    @ManyToOne
    @JoinColumn(name = "type_documents_code")
    private TypeDocument typeDocument;

    @ManyToOne
    @JoinColumn(name = "dossiers_id")
    private Dossier dossier;
}
