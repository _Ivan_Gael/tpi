package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.DayOfWeek;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "jours")
public class Jour implements Serializable {

    @Id
    private String code;

    @Column(nullable = false)
    private String libelle;


    public DayOfWeek toDayOfWeek(){
        switch(this.code){
            case "LUNDI":
                return DayOfWeek.MONDAY;
            case "MARDI":
                return DayOfWeek.TUESDAY;
            case "MERCREDI":
                return DayOfWeek.WEDNESDAY;
            case "JEUDI":
                return DayOfWeek.THURSDAY;
            case "VENDREDI":
                return DayOfWeek.FRIDAY;
            case "SAMEDI":
                return DayOfWeek.SATURDAY;
            case "DIMANCHE":
                return DayOfWeek.SUNDAY;
            default:
                return DayOfWeek.SUNDAY;
        }
    }
}
