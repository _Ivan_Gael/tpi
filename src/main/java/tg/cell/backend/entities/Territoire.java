package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "territoires")
public class Territoire implements Serializable {

    @Id
    private String code;

    private String libelle;

    @ManyToOne
    @JoinColumn(name = "type_territoires_code")
    private TypeTerritoire typeTerritoire;

    @ManyToOne
    @JoinColumn(name = "territoires_code")
    private Territoire territoire;


    @Override
    public String toString() {
        return "code : " + this.code + " libelle :"+ this.libelle + " typeTerritoire : "+ this.typeTerritoire;
    }
}
