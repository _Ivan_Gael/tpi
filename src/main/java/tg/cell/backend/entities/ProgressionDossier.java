package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "progression_dossiers")
public class ProgressionDossier implements Serializable {

    @Id
    private String code;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "statuts_code")
    private Statut statut;

    @ManyToOne
    @JoinColumn(name = "evenements_code")
    private Evenement evenement;

    @ManyToOne
    @JoinColumn(name = "dossiers_id")
    private Dossier dossier;


}
