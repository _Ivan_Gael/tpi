package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import reactor.util.annotation.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "personnes")
public class Personne implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nom;

    private String prenom;

    private String sexe;

    private Date dateNaiss;

    private String numeroIdentification;

    private String tel;

    private String email;

    @ManyToOne
    @JoinColumn(name = "professions_id")
    private Profession profession;

    private String adresse;

    @ManyToOne
    @JoinColumn(name = "personnes_pere_id",nullable = true)
    private Personne pere;

    @ManyToOne
    @JoinColumn(name = "personnes_mere_id", nullable = true)
    private Personne mere;

    @ManyToOne
    @JoinColumn(name = "territoires_code")
    private Territoire territoire;





}