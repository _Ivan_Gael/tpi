package tg.cell.backend.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "type_acte_documents")
public class TypeActeDocument implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Boolean isRequired;

    @ManyToOne
    @JoinColumn(name = "type_actes_code")
    @JsonIgnore
    private TypeActe typeActe;

    @ManyToOne
    @JoinColumn(name = "type_documents_code")
    private TypeDocument typeDocument;

    @Transient
    private String typeActeOtherCode;
}
