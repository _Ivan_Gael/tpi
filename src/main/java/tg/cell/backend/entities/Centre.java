package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "centres")
public class Centre implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String libelle;

    private String tel;

    private String email;

    private String boitePostale;

    private String adressePhysique;

    @ManyToOne
    @JoinColumn(name = "territoires_code")
    private Territoire territoire;
}
