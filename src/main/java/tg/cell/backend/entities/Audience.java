package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "audiences")
public class Audience implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Date date;

    @Temporal(TemporalType.TIME)
    private Date heure;

    @ManyToOne
    @JoinColumn(name = "nature_audiences_code")
    private NatureAudience natureAudience;

    @ManyToOne
    @JoinColumn(name = "dossiers_id")
    private Dossier dossier;

    private String serviceJuridictionCode;

}
