package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "dossiers")
public class Dossier implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    private String numeroRG;

    
    private Date dateDemande;

    @Column(nullable = true)
    private Date dateSaisine;

    @ManyToOne
    @JoinColumn(name = "annees_code")
    private Annee annee;

    @ManyToOne
    @JoinColumn(name = "statuts_code")
    private Statut statut;

    @ManyToOne
    @JoinColumn(name = "evenement_code")
    private Evenement evenement;

    @ManyToOne
    @JoinColumn(name = "personnes_id")
    Personne demandeur;

    @ManyToOne
    @JoinColumn(name = "type_actes_code")
    private TypeActe typeActe;
}
