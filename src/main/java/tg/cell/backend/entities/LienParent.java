package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "lien_parents")
public class LienParent implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "enfant_id")
    private Personne enfant;

    @ManyToOne
    @JoinColumn(name = "pere_id")
    private Personne pere;

    @ManyToOne
    @JoinColumn(name = "mere_id")
    private Personne mere;
}
