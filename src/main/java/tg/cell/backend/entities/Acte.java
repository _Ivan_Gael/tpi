package tg.cell.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "actes")
public class Acte implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String numero;

    private String numeroFeuillet;

    private String numeroRegistre;

    @Column(nullable = false)
    private Date date;

    @ManyToOne
    @JoinColumn(name = "centres_id")
    private Centre centre;

    @ManyToOne
    @JoinColumn(name = "dossiers_id")
    private Dossier dossier;
}
