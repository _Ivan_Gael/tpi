package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.TypeActe;
import tg.cell.backend.entities.TypeActeDocument;

import java.util.List;

@Repository
public interface TypeActeDocumentRepository extends JpaRepository<TypeActeDocument, Long> {

    List<TypeActeDocument> findByTypeActe(TypeActe typeActe);
}