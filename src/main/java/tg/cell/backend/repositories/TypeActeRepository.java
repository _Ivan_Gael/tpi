package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.TypeActe;

@Repository
public interface TypeActeRepository extends JpaRepository<TypeActe, String> {
}