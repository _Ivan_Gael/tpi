package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Partie;

@Repository
public interface PartieRepository extends JpaRepository<Partie, Long> {
}