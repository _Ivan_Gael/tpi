package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Jour;

@Repository
public interface JourRepository extends JpaRepository<Jour, String> {
}