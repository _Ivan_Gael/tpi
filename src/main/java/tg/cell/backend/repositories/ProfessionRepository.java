package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import tg.cell.backend.entities.Personne;
import tg.cell.backend.entities.Profession;

public interface ProfessionRepository extends JpaRepository<Profession, Long> {
}
