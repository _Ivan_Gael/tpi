package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Centre;

import java.util.Optional;

@Repository
public interface CentreRepository extends JpaRepository<Centre, Long> {

    Optional<Centre> findByLibelle(String libelle);
}