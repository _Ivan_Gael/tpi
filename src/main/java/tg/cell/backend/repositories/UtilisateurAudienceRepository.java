package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.UtilisateurAudience;

@Repository
public interface UtilisateurAudienceRepository extends JpaRepository<UtilisateurAudience, Long> {
}