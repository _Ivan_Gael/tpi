package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Personne;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Long> {
}