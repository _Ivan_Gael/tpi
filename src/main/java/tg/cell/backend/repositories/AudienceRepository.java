package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Audience;

@Repository
public interface AudienceRepository extends JpaRepository<Audience, Long> {
}