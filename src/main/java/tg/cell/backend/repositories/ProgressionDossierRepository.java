package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.ProgressionDossier;

@Repository
public interface ProgressionDossierRepository extends JpaRepository<ProgressionDossier, String> {
}