package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.TypeTerritoire;

import java.util.Optional;

@Repository
public interface TypeTerritoireRepository extends JpaRepository<TypeTerritoire, String> {

    Optional<TypeTerritoire> findByLibelle(String libelle);
}