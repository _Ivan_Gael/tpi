package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.TypePartie;

@Repository
public interface TypePartieRepository extends JpaRepository<TypePartie, String> {
}