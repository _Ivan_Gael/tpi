package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Centre;
import tg.cell.backend.entities.Compteur;

@Repository
public interface CompteurRepository extends JpaRepository<Compteur, String> {
}
