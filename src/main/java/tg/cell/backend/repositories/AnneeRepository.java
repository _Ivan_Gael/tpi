package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Annee;

@Repository
public interface AnneeRepository extends JpaRepository<Annee, String> {
}