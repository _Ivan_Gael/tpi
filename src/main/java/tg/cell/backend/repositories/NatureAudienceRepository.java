package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.NatureAudience;

@Repository
public interface NatureAudienceRepository extends JpaRepository<NatureAudience, String> {
}