package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.ProgrammationAudience;

@Repository
public interface ProgrammationAudienceRepository extends JpaRepository<ProgrammationAudience, Long> {
}