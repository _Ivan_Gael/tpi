package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Periode;

@Repository
public interface PeriodeRepository extends JpaRepository<Periode, String> {
}