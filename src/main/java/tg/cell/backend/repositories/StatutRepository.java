package tg.cell.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tg.cell.backend.entities.Statut;

@Repository
public interface StatutRepository extends JpaRepository<Statut, String> {
}