package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypePartie;
import tg.cell.backend.repositories.TypePartieRepository;
import tg.cell.backend.services.TypePartieService;

import java.util.List;

@Service
public class TypePartieServiceImpl implements TypePartieService {

    private final TypePartieRepository typePartieRepository;


    public TypePartieServiceImpl(TypePartieRepository typePartieRepository) {
        this.typePartieRepository = typePartieRepository;
    }

    @Override
    public List<TypePartie> findAll() {
        List<TypePartie> typeParties = this.typePartieRepository.findAll();
        return typeParties;
    }

    @Override
    public TypePartie findByCode(String code) {
        TypePartie typePartie = this.typePartieRepository.findById(code).orElse(null);
        return typePartie;
    }

    @Override
    public TypePartie save(TypePartie typePartie) {
        TypePartie saved = this.typePartieRepository.save(typePartie);
        return saved;
    }

    @Override
    public TypePartie update(TypePartie typePartie, String id) {
        TypePartie typePartieFound = this.typePartieRepository.findById(id).orElse(null);
        if (typePartieFound == null) {
            return null;
        }
        typePartie.setLibelle(typePartie.getLibelle());
        TypePartie updated = this.typePartieRepository.save(typePartie);
        return updated;
    }
}
