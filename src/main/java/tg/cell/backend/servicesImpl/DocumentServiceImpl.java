package tg.cell.backend.servicesImpl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Document;
import tg.cell.backend.entities.TypeDocument;
import tg.cell.backend.repositories.DocumentRepository;
import tg.cell.backend.services.DocumentService;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    @Value("${chemin.base.fichiers}")
    private String basePath;



    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public List<Document> findAll() {
        return this.documentRepository.findAll();
    }

    @Override
    public Document findBydId(Long id) {
        Document doc = this.documentRepository.findById(id).orElse(null);
        return doc;
    }

    @Override
    public Document save(MultipartFile fichier) {
        try{
            /*ObjectMapper mapper = new ObjectMapper();*/
            Fonctions f = new Fonctions();
            InputStream inputStream = fichier.getInputStream();
            Document doc = new Document();
            doc.setDate(new Date());
            doc.setNom(f.generateFileName(fichier));
            Date date = new Date();
            SimpleDateFormat DateFor = new SimpleDateFormat("yyyy/MM");
            doc.setUploadFileName(fichier.getOriginalFilename());
            doc.setTaille(fichier.getSize());
            doc = documentRepository.save(doc);
            String path = DateFor.format(date) + "/" + doc.getId();
            f.setPath(path);
            f.setBasePath(basePath);
            f.setMultipartFile(fichier);
            doc.setRepertoire(basePath + "/" + path);
            doc = documentRepository.save(doc);
            f.setPath(path);
            f.setImportation(doc);
            if (f.transfertFile()) {
                System.err.println("saving data ...");
                doc.setDateDebutImportation(new Date());
                doc = documentRepository.save(doc);
                doc.setDateFinImportation(new Date());
                Document savedDoc = documentRepository.save(doc);
                return savedDoc;
            }
            //  return new ResponseEntity<>("Données importées avec succès", HttpStatus.OK);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Document saveDoc(Document document) {
        return this.documentRepository.save(document);
    }

    @Override
    public Document update(Document document, Long id) {
        Document doc = this.documentRepository.findById(id).orElse(null);
        return null;
    }
}
