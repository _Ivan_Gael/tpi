package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Centre;
import tg.cell.backend.repositories.CentreRepository;
import tg.cell.backend.services.CentreService;

import java.util.List;

@Service
public class CentreServiceImpl implements CentreService {

    private final CentreRepository centreRepository;

    public CentreServiceImpl(CentreRepository centreRepository) {
        this.centreRepository = centreRepository;
    }

    @Override
    public List<Centre> findAll() {
        List<Centre> centres = this.centreRepository.findAll();
        return centres;
    }

    @Override
    public Centre findById(Long id) {
        Centre centre = this.centreRepository.findById(id).orElse(null);
        return centre;
    }

    @Override
    public Centre findByLibelle(String libelle) {
        Centre centre = this.centreRepository.findByLibelle(libelle).orElse(null);
        return centre;
    }

    @Override
    public Centre save(Centre centre) {
        Centre saved = this.centreRepository.save(centre);
        return saved;
    }

    @Override
    public Centre update(Centre centre, Long id) {
        Centre centreFound = this.centreRepository.findById(id).orElse(null);
        if(centreFound == null){
            return null;
        }
        centreFound.setLibelle(centre.getLibelle());
        centreFound.setEmail(centre.getEmail());
        centreFound.setAdressePhysique(centre.getAdressePhysique());
        centreFound.setBoitePostale(centre.getBoitePostale());
        centreFound.setTel(centre.getTel());
        centreFound.setTerritoire(centre.getTerritoire());
        Centre updated = this.centreRepository.save(centreFound);
        return updated;
    }


}
