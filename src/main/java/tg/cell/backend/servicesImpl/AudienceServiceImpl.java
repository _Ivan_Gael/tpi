package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Audience;
import tg.cell.backend.repositories.AudienceRepository;
import tg.cell.backend.services.AudienceService;

import java.util.List;

@Service
public class AudienceServiceImpl implements AudienceService {

    private final AudienceRepository audienceRepository;

    public AudienceServiceImpl(AudienceRepository audienceRepository) {
        this.audienceRepository = audienceRepository;
    }

    @Override
    public List<Audience> findAll() {
        List<Audience> audiences = this.audienceRepository.findAll();
        return audiences;
    }

    @Override
    public Audience findById(Long id) {
        Audience audience = this.audienceRepository.findById(id).orElse(null);
        return audience;
    }

    @Override
    public Audience save(Audience audience) {
        audience.setServiceJuridictionCode("4-CH");
        Audience saved = this.audienceRepository.save(audience);
        return saved;
    }

    @Override
    public Audience update(Audience audience, Long id) {
        Audience audienceFound = this.audienceRepository.findById(id).orElse(null);
        if (audienceFound == null) {
            return null;
        }
        audienceFound.setNatureAudience(audience.getNatureAudience());
        audienceFound.setDate(audience.getDate());
        audienceFound.setDossier(audience.getDossier());
        audienceFound.setServiceJuridictionCode(audience.getServiceJuridictionCode());
        Audience updated = this.audienceRepository.save(audience);
        return updated;
    }
}
