package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.ProgressionDossier;
import tg.cell.backend.repositories.ProgressionDossierRepository;
import tg.cell.backend.repositories.StatutRepository;
import tg.cell.backend.services.ProgressionDossierService;

import java.util.Date;
import java.util.List;

@Service
public class ProgressionDossierServiceImpl implements ProgressionDossierService {

    private final ProgressionDossierRepository progressionDossierRepository;

    private final StatutRepository statutRepository;

    public ProgressionDossierServiceImpl(ProgressionDossierRepository progressionDossierRepository, StatutRepository statutRepository) {
        this.progressionDossierRepository = progressionDossierRepository;
        this.statutRepository = statutRepository;
    }

    @Override
    public List<ProgressionDossier> findAll() {
        List<ProgressionDossier> progressionDossiers = this.progressionDossierRepository.findAll();
        return progressionDossiers;
    }

    @Override
    public ProgressionDossier findByCode(String code) {
        ProgressionDossier progressionDossier = this.progressionDossierRepository.findById(code).orElse(null);
        return progressionDossier;
    }

    @Override
    public ProgressionDossier save(ProgressionDossier progressionDossier) {
        Long count = this.progressionDossierRepository.count();
        progressionDossier.setCode(progressionDossier.getStatut().getCode() + "-" + count.toString());
        progressionDossier.setDate(new Date());
        ProgressionDossier saved = this.progressionDossierRepository.save(progressionDossier);
        return saved;
    }

    @Override
    public ProgressionDossier update(ProgressionDossier progressionDossier, String code) {
        ProgressionDossier progressionDossierFound = this.progressionDossierRepository.findById(code).orElse(null);
        if (progressionDossierFound == null) {
            return null;
        }
        progressionDossierFound.setDate(new Date());
        progressionDossierFound.setStatut(progressionDossier.getStatut());
        progressionDossierFound.setEvenement(progressionDossier.getEvenement());
        ProgressionDossier updated = this.progressionDossierRepository.save(progressionDossier);
        return updated;
    }
}
