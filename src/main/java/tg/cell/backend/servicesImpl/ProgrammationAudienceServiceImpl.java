package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.ProgrammationAudience;
import tg.cell.backend.repositories.ProgrammationAudienceRepository;
import tg.cell.backend.services.ProgrammationAudienceService;

import java.util.Date;
import java.util.List;

@Service
public class ProgrammationAudienceServiceImpl implements ProgrammationAudienceService {

    private final ProgrammationAudienceRepository programmationAudienceRepository;


    public ProgrammationAudienceServiceImpl(ProgrammationAudienceRepository programmationAudienceRepository) {
        this.programmationAudienceRepository = programmationAudienceRepository;
    }

    @Override
    public List<ProgrammationAudience> findAll() {
        List<ProgrammationAudience> programmationAudiences = this.programmationAudienceRepository.findAll();
        return programmationAudiences;
    }

    @Override
    public ProgrammationAudience findById(Long id) {
        ProgrammationAudience programmationAudience = this.programmationAudienceRepository.findById(id).orElse(null);
        return programmationAudience;
    }

    @Override
    public ProgrammationAudience save(ProgrammationAudience programmationAudience) {
        ProgrammationAudience saved = this.programmationAudienceRepository.save(programmationAudience);
        return saved;
    }

    @Override
    public ProgrammationAudience update(ProgrammationAudience programmationAudience, Long id) {
        ProgrammationAudience programmationAudienceFound = this.programmationAudienceRepository.findById(id).orElse(null);
        if (programmationAudienceFound == null) {
            return null;
        }
        programmationAudienceFound.setDate(new Date());
        programmationAudienceFound.setJour(programmationAudience.getJour());
        programmationAudienceFound.setServiceJuridictionCode(programmationAudience.getServiceJuridictionCode());
        programmationAudienceFound.setPeriode(programmationAudience.getPeriode());
        ProgrammationAudience updated = this.programmationAudienceRepository.save(programmationAudience);
        return updated;
    }
}
