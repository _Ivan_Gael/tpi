package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.UtilisateurAudience;
import tg.cell.backend.repositories.UtilisateurAudienceRepository;
import tg.cell.backend.services.UtilisateurAudienceService;

import java.util.Date;
import java.util.List;

@Service
public class UtilisateurAudienceServiceImpl implements UtilisateurAudienceService {

    private final UtilisateurAudienceRepository utilisateurAudienceRepository;

    public UtilisateurAudienceServiceImpl(UtilisateurAudienceRepository utilisateurAudienceRepository) {
        this.utilisateurAudienceRepository = utilisateurAudienceRepository;
    }

    @Override
    public List<UtilisateurAudience> findAll() {
        List<UtilisateurAudience> utilisateurAudiences = this.utilisateurAudienceRepository.findAll();
        return utilisateurAudiences;
    }

    @Override
    public UtilisateurAudience findById(Long id) {
        UtilisateurAudience utilisateurAudience = this.utilisateurAudienceRepository.findById(id).orElse(null);
        return utilisateurAudience;
    }

    @Override
    public UtilisateurAudience save(UtilisateurAudience utilisateurAudience) {
        UtilisateurAudience saved = this.utilisateurAudienceRepository.save(utilisateurAudience);
        return saved;
    }

    @Override
    public UtilisateurAudience update(UtilisateurAudience utilisateurAudience, Long id) {
        UtilisateurAudience utilisateurAudienceFound = this.utilisateurAudienceRepository.findById(id).orElse(null);
        if (utilisateurAudienceFound == null) {
            return null;
        }
        utilisateurAudience.setAudience(utilisateurAudience.getAudience());
        utilisateurAudience.setUtilisateurId(utilisateurAudience.getUtilisateurId());
        utilisateurAudience.setDate(new Date());
        UtilisateurAudience updated = this.utilisateurAudienceRepository.save(utilisateurAudience);
        return updated;
    }
}
