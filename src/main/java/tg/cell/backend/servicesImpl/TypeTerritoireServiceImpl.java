package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypeTerritoire;
import tg.cell.backend.repositories.TypeTerritoireRepository;
import tg.cell.backend.services.TypeTerritoireService;

import java.util.List;

@Service
public class TypeTerritoireServiceImpl implements TypeTerritoireService {

    private final TypeTerritoireRepository typeTerritoireRepository;


    public TypeTerritoireServiceImpl(TypeTerritoireRepository typeTerritoireRepository) {
        this.typeTerritoireRepository = typeTerritoireRepository;
    }

    @Override
    public List<TypeTerritoire> findAll() {
        List<TypeTerritoire> typeTerritoires = this.typeTerritoireRepository.findAll();
        return typeTerritoires;
    }

    @Override
    public TypeTerritoire findByCode(String code) {
        TypeTerritoire typeTerritoire = this.typeTerritoireRepository.findById(code).orElse(null);
        return typeTerritoire;
    }

    @Override
    public TypeTerritoire save(TypeTerritoire typeTerritoire) {
        TypeTerritoire saved = this.typeTerritoireRepository.save(typeTerritoire);
        return saved;
    }

    @Override
    public TypeTerritoire update(TypeTerritoire typeTerritoire, String code) {
        TypeTerritoire typeTerritoireFound = this.typeTerritoireRepository.findById(code).orElse(null);
        if (typeTerritoireFound == null){
            return null;
        }
        typeTerritoire.setLibelle(typeTerritoire.getLibelle());
        TypeTerritoire updated = this.typeTerritoireRepository.save(typeTerritoire);
        return updated;
    }
}
