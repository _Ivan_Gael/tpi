package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypeActe;
import tg.cell.backend.repositories.TypeActeRepository;
import tg.cell.backend.services.TypeActeService;

import java.util.List;

@Service
public class TypeActeServiceImpl implements TypeActeService {

    private final TypeActeRepository typeActeRepository;


    public TypeActeServiceImpl(TypeActeRepository typeActeRepository) {
        this.typeActeRepository = typeActeRepository;
    }

    @Override
    public List<TypeActe> findAll() {
        List<TypeActe> typeActes = this.typeActeRepository.findAll();
        return typeActes;
    }

    @Override
    public TypeActe findByCode(String code) {
        TypeActe typeActe = this.typeActeRepository.findById(code).orElse(null);
        return typeActe;
    }

    @Override
    public TypeActe save(TypeActe typeActe) {
        TypeActe saved = this.typeActeRepository.save(typeActe);
        return saved;
    }

    @Override
    public TypeActe update(TypeActe typeActe, String code) {
        TypeActe typeActeFound = this.typeActeRepository.findById(code).orElse(null);
        if (typeActeFound == null) {
            return null;
        }
        typeActe.setLibelle(typeActe.getLibelle());
        typeActe.setFrais(typeActe.getFrais());
        TypeActe updated = this.typeActeRepository.save(typeActe);
        return updated;
    }
}
