package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypeActe;
import tg.cell.backend.entities.TypeActeDocument;
import tg.cell.backend.repositories.TypeActeDocumentRepository;
import tg.cell.backend.repositories.TypeActeRepository;
import tg.cell.backend.services.TypeActeDocumentService;

import java.util.List;

@Service
public class TypeActeDocumentServiceImpl implements TypeActeDocumentService {

    private final TypeActeDocumentRepository typeActeDocumentRepository;

    private final TypeActeRepository typeActeRepository;

    public TypeActeDocumentServiceImpl(TypeActeDocumentRepository typeActeDocumentRepository, TypeActeRepository typeActeRepository) {
        this.typeActeDocumentRepository = typeActeDocumentRepository;
        this.typeActeRepository = typeActeRepository;
    }

    @Override
    public List<TypeActeDocument> findAll() {
        List<TypeActeDocument> typeActeDocuments = this.typeActeDocumentRepository.findAll();
        return typeActeDocuments;
    }

    @Override
    public TypeActeDocument findById(Long id) {
        TypeActeDocument typeActeDocument = this.typeActeDocumentRepository.findById(id).orElse(null);
        return typeActeDocument;
    }

    @Override
    public List<TypeActeDocument> findByTypeActe(TypeActe typeActe) {
        List<TypeActeDocument> typeActeDocuments = this.typeActeDocumentRepository.findByTypeActe(typeActe);
        return typeActeDocuments;
    }

    @Override
    public TypeActeDocument save(TypeActeDocument typeActeDocument) {
        TypeActe typeActe = this.typeActeRepository.findById(typeActeDocument.getTypeActeOtherCode()).orElse(null);
        typeActeDocument.setTypeActe(typeActe);
        TypeActeDocument saved = this.typeActeDocumentRepository.save(typeActeDocument);
        return saved;
    }

    @Override
    public TypeActeDocument update(TypeActeDocument typeActeDocument, Long id) {
        TypeActeDocument typeActeDocumentFound = this.typeActeDocumentRepository.findById(id).orElse(null);
        if (typeActeDocumentFound == null) {
            return null;
        }
        TypeActe typeActe = this.typeActeRepository.findById(typeActeDocument.getTypeActeOtherCode()).orElse(null);
        typeActeDocumentFound.setTypeActe(typeActe);
        typeActeDocumentFound.setTypeDocument(typeActeDocument.getTypeDocument());
        typeActeDocumentFound.setIsRequired(typeActeDocument.getIsRequired());
        TypeActeDocument updated = this.typeActeDocumentRepository.save(typeActeDocumentFound);
        return updated;
    }
}
