package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Annee;
import tg.cell.backend.repositories.AnneeRepository;
import tg.cell.backend.services.AnneeService;

import java.util.List;

@Service
public class AnneeServiceImpl implements AnneeService {

    private final AnneeRepository anneeRepository;

    public AnneeServiceImpl(AnneeRepository anneeRepository) {
        this.anneeRepository = anneeRepository;
    }

    @Override
    public List<Annee> findAll() {
        List<Annee> annees = this.anneeRepository.findAll();
        return annees;
    }

    @Override
    public Annee findByCode(String code) {
        Annee annee = this.anneeRepository.findById(code).orElse(null);
        return annee;
    }

    @Override
    public Annee save(Annee annee) {
        Annee saved = this.anneeRepository.save(annee);
        return saved;
    }

    @Override
    public Annee update(Annee annee, String code) {
        Annee anneeFound = this.anneeRepository.findById(code).orElse(null);
        if (anneeFound == null) {
            return null;
        }
        anneeFound.setLibelle(annee.getLibelle());
        Annee updated = this.anneeRepository.save(annee);
        return updated;
    }
}
