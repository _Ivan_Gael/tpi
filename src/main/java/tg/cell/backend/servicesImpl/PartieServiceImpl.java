package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Partie;
import tg.cell.backend.repositories.PartieRepository;
import tg.cell.backend.services.PartieService;

import java.util.Date;
import java.util.List;

@Service
public class PartieServiceImpl implements PartieService {

    private final PartieRepository partieRepository;

    public PartieServiceImpl(PartieRepository partieRepository) {
        this.partieRepository = partieRepository;
    }

    @Override
    public List<Partie> findAll() {
        List<Partie> parties = this.partieRepository.findAll();
        return parties;
    }

    @Override
    public Partie findById(Long id) {
        Partie partie = this.partieRepository.findById(id).orElse(null);
        return partie;
    }

    @Override
    public Partie save(Partie partie) {
        Partie saved = this.partieRepository.save(partie);
        return saved;
    }

    @Override
    public Partie update(Partie partie, Long id) {
        Partie partieFound = this.partieRepository.findById(id).orElse(null);
        if (partieFound == null) {
            return null;
        }
        partieFound.setDate(new Date());
        partieFound.setTypePartie(partie.getTypePartie());
        partieFound.setDemandeur(partie.getDemandeur());
        Partie updated = this.partieRepository.save(partie);
        return updated;
    }
}
