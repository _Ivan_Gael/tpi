package tg.cell.backend.servicesImpl;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.TypeDocument;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FileModel {
    private MultipartFile multipartFile;
    private TypeDocument typeDocument;
}
