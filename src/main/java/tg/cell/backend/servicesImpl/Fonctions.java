package tg.cell.backend.servicesImpl;

import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Document;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Fonctions {
    private File file;
    private String basePath;
    private String path;
    private MultipartFile multipartFile;
    private Document document;

    public Fonctions() {
    }

    public Fonctions(File file, String basePath, String path, MultipartFile multipartFile, Document document) {
        this.file = file;
        this.basePath = basePath;
        this.path = path;
        this.multipartFile = multipartFile;
        this.document = document;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }

    public Document getImportation() {
        return document;
    }

    public void setImportation(Document document) {
        this.document = document;
    }

    public boolean existe() {
        if (file == null) {
            return false;
        }
        if (file.length() <= 0) {
            return false;
        }
        return file.exists();
    }

    public boolean isReadable() {
        if (!existe()) {
            return false;
        }
        return Files.isReadable(file.toPath());
    }

    public boolean createFolder(String folders) {
        if (folders == null || folders.isEmpty()) {
            return false;
        }
        String subFolders[] = folders.split("/");
        int subFolderNumber = subFolders.length;
        String currentFolder = basePath ;
        System.err.println(currentFolder + " ***");
        int createdFolderNumber = 0;
        int existentFolderNumber = 0;
        for (int i = 0; i < subFolderNumber; i++) {
            currentFolder = currentFolder + "/" + subFolders[i];
            File file = new File(currentFolder);
            System.err.println(currentFolder + " IN");
            if (!file.exists()) {
                if (file.mkdir()) {
                    createdFolderNumber++;
                    System.err.println(currentFolder + " Ivan");
                }
            } else {
                existentFolderNumber++;
            }

        }
        path = currentFolder;
        return ((createdFolderNumber + existentFolderNumber) == subFolderNumber);
    }

    public boolean transfertFile()  {
      try {
          if (multipartFile==null){
              return  false;
          }
          if(createFolder(path)){

              Path chemin = Paths.get(this.path + "/" + document.getNom());
              file = new File(chemin.toString());
              System.err.println("chemin === "+chemin.toString());
              multipartFile.transferTo(file);
              return  existe() && isReadable();
          }else {
              return  false;
          }
      }catch (Exception e){
          e.printStackTrace();
          return  false;
      }

    }

    public String generateFileName(MultipartFile document) {
        String extension = "";
        String result = "";
        String[] ext = document.getOriginalFilename().split("\\.");
        if (ext.length > 0) {
            extension = ext[ext.length - 1];
        } else {
            extension = "";
        }

        result = getFileName(extension);

        return result;
    }

    public String getFileName(String extension) {
        String retour = new SimpleDateFormat("ddMMyyyyHHmmSSsss", Locale.FRENCH).format(new Date());

        if (!((extension == null) || extension.equals(""))) {
            retour += "." + extension;
        }

        return retour;
    }
}
