package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.LienParent;
import tg.cell.backend.repositories.LienParentRepository;
import tg.cell.backend.services.LienParenteService;

import java.util.List;

@Service
public class LienParenteServiceImpl implements LienParenteService {

    private final LienParentRepository lienParentRepository;


    public LienParenteServiceImpl(LienParentRepository lienParentRepository) {
        this.lienParentRepository = lienParentRepository;
    }

    @Override
    public List<LienParent> findAll() {
        List<LienParent> lienParents = this.lienParentRepository.findAll();
        return lienParents;
    }

    @Override
    public LienParent findById(Long id) {
        LienParent lienParent = this.lienParentRepository.findById(id).orElse(null);
        return lienParent;
    }

    @Override
    public LienParent save(LienParent lienParent) {
        LienParent saved = this.lienParentRepository.save(lienParent);
        return saved;
    }

    @Override
    public LienParent update(LienParent lienParent, Long id) {
        LienParent lienParentFound = this.lienParentRepository.findById(id).orElse(null);
        if (lienParentFound == null){
            return null;
        }
        lienParentFound.setEnfant(lienParent.getEnfant());
        lienParentFound.setPere(lienParent.getPere());
        lienParentFound.setMere(lienParent.getMere());
        LienParent updated = this.lienParentRepository.save(lienParent);
        return updated;
    }
}
