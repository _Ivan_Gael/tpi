package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Acte;
import tg.cell.backend.repositories.ActeRepository;
import tg.cell.backend.services.ActeService;

import java.util.Date;
import java.util.List;

@Service
public class ActeServiceImpl implements ActeService {

    private final ActeRepository acteRepository;

    public ActeServiceImpl(ActeRepository acteRepository) {
        this.acteRepository = acteRepository;
    }

    @Override
    public List<Acte> findAll() {
        List<Acte> actes = this.acteRepository.findAll();
        return actes;
    }

    @Override
    public Acte findById(Long id) {
        Acte acte = this.acteRepository.findById(id).orElse(null);
        return acte;
    }

    @Override
    public Acte save(Acte acte) {
        Acte saved = this.acteRepository.save(acte);
        return saved;
    }

    @Override
    public Acte update(Acte acte, Long id) {
        Acte acteFound = this.acteRepository.findById(id).orElse(null);
        if (acteFound == null) {
            return null;
        }
        acteFound.setNumero(acte.getNumero());
        acteFound.setDate(new Date());
        acteFound.setDossier(acte.getDossier());
        acteFound.setCentre(acte.getCentre());
        acteFound.setNumeroFeuillet(acte.getNumeroFeuillet());
        acteFound.setNumeroRegistre(acte.getNumeroRegistre());
        Acte updated = this.acteRepository.save(acte);
        return updated;
    }
}
