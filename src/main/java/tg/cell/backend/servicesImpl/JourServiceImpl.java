package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Jour;
import tg.cell.backend.repositories.JourRepository;
import tg.cell.backend.services.JourService;

import java.util.List;

@Service
public class JourServiceImpl implements JourService {

    private final JourRepository jourRepository;

    public JourServiceImpl(JourRepository jourRepository) {
        this.jourRepository = jourRepository;
    }

    @Override
    public List<Jour> findAll() {
        List<Jour> jours = this.jourRepository.findAll();
        return jours;
    }

    @Override
    public Jour findByCode(String code) {
        Jour jour = this.jourRepository.findById(code).orElse(null);
        return jour;
    }

    @Override
    public Jour save(Jour jour) {
        Jour saved = this.jourRepository.save(jour);
        return saved;
    }

    @Override
    public Jour update(Jour jour, String code) {
        Jour jourFound = this.jourRepository.findById(code).orElse(null);
        if (jourFound == null) {
            return null;
        }
        jourFound.setLibelle(jour.getLibelle());
        Jour updated = this.jourRepository.save(jour);
        return updated;
    }
}
