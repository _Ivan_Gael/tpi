package tg.cell.backend.servicesImpl;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.*;
import tg.cell.backend.repositories.*;
import tg.cell.backend.services.DocumentService;
import tg.cell.backend.services.PersonneService;
import tg.cell.backend.services.ProgressionDossierService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class PersonneServiceImpl implements PersonneService {

    private final PersonneRepository personneRepository;

    private final LienParentRepository lienParentRepository;

    private final PartieRepository partieRepository;

    private final DossierRepository dossierRepository;

    private final DocumentService documentService;

    private final TypePartieRepository typePartieRepository;

    @Autowired
    private CompteurService compteurService;

    @Autowired
    private AnneeRepository anneeRepository;

    @Autowired
    private StatutRepository statutRepository;

    @Autowired
    private EvenementRepository evenementRepository;

    @Autowired
    private ProgressionDossierRepository progressionDossierRepository;

    @Autowired
    private ProgressionDossierService progressionDossierService;

    public PersonneServiceImpl(PersonneRepository personneRepository, LienParentRepository lienParentRepository, PartieRepository partieRepository, DossierRepository dossierRepository, DocumentService documentService, TypePartieRepository typePartieRepository) {
        this.personneRepository = personneRepository;
        this.lienParentRepository = lienParentRepository;
        this.partieRepository = partieRepository;
        this.dossierRepository = dossierRepository;
        this.documentService = documentService;
        this.typePartieRepository = typePartieRepository;
    }

    @Override
    public List<Personne> findAll() {
        List<Personne> personnes = this.personneRepository.findAll();
        return personnes;
    }

    @Override
    public Personne findById(Long id) {
        Personne personne = this.personneRepository.findById(id).orElse(null);
        return personne;
    }

    @Override
    public Personne save(Personne personne) {
        Personne saved = this.personneRepository.save(personne);
        return saved;
    }

    @Override
    public Boolean saveAllProcess(String demandeur, String typeActe,String[] typeDocuments, MultipartFile[] files) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            Personne demandeurInfo = mapper.readValue(demandeur, Personne.class);
            Personne savedDem = this.personneRepository.save(demandeurInfo);
            Dossier dossier = new Dossier();
            dossier.setDateDemande(new Date());
            dossier.setDateSaisine(new Date());
            dossier.setDemandeur(savedDem);
            TypeActe typeActe1 = mapper.readValue(typeActe,TypeActe.class);
            dossier.setTypeActe(typeActe1);
            Dossier savedDossier = this.dossierRepository.save(dossier);
            String year = new SimpleDateFormat("yyyy").format(savedDossier.getDateSaisine());
            String rg = compteurService.generateRG("NUMERORG-" + "TPI-LOME", "1101", savedDossier.getDateSaisine());
            String code = "TPI-LOME" + year;
            savedDossier.setNumeroRG(rg);
           /* ProgressionDossier progressionDossier = new ProgressionDossier();
            progressionDossier.setDossier(savedDossier);*/
            Statut statut = this.statutRepository.findById("NEW").orElse(null);
            savedDossier.setStatut(statut);
            /*progressionDossier.setStatut(statut);*/
            Evenement evenement = this.evenementRepository.findByLibelle("CREATION DE DOSSIER").orElse(null);
            /*progressionDossier.setEvenement(evenement);*/
            savedDossier.setEvenement(evenement);
            /*this.progressionDossierService.save(progressionDossier);*/
            Annee annee = anneeRepository.findById(code).orElse(null);
            if (annee == null) {
                annee = new Annee(code, year, "TPI-LOME");
                Annee anneeSaved = anneeRepository.save(annee);
                savedDossier.setAnnee(anneeSaved);
                this.dossierRepository.save(savedDossier);
            }
            savedDossier.setAnnee(annee);
            Dossier ssDossier = this.dossierRepository.save(savedDossier);
            for(MultipartFile f : files){
                Document savedDocument = this.documentService.save(f);
                savedDocument.setDossier(ssDossier);
                Document ssdoc = this.documentService.saveDoc(savedDocument);

                for(String t : typeDocuments){
                    ObjectMapper m = new ObjectMapper();
                    TypeDocument typ = m.readValue(t,TypeDocument.class);
                    ssdoc.setTypeDocument(typ);
                    this.documentService.saveDoc(ssdoc);
                }

            }

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Personne update(Personne personne, Long id) {
        Personne personneFound = this.personneRepository.findById(id).orElse(null);
        if (personneFound == null) {
            return null;
        }
        personneFound.setDateNaiss(personne.getDateNaiss());
        personneFound.setNom(personne.getNom());
        personneFound.setPrenom(personne.getPrenom());
        personneFound.setSexe(personne.getSexe());
        personneFound.setProfession(personne.getProfession());
        personneFound.setAdresse(personne.getAdresse());
        personneFound.setTel(personne.getTel());
        personneFound.setEmail(personne.getEmail());
        personneFound.setNumeroIdentification(personne.getNumeroIdentification());
        personneFound.setPere(personne.getPere());
        personneFound.setMere(personne.getMere());
        personneFound.setTerritoire(personne.getTerritoire());
        Personne updated = this.personneRepository.save(personne);
        return updated;
    }
}
