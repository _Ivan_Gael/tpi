package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Territoire;
import tg.cell.backend.repositories.TerritoireRepository;
import tg.cell.backend.services.TerritoireService;

import java.util.List;

@Service
public class TerritoireServiceImpl implements TerritoireService {

    private final TerritoireRepository territoireRepository;

    public TerritoireServiceImpl(TerritoireRepository territoireRepository) {
        this.territoireRepository = territoireRepository;
    }

    @Override
    public List<Territoire> findAll() {
        List<Territoire> territoires = this.territoireRepository.findAll();
        return territoires;
    }

    @Override
    public Territoire findByCode(String code) {
        Territoire territoire = this.territoireRepository.findById(code).orElse(null);
        return territoire;
    }

    @Override
    public Territoire save(Territoire territoire) {
        Territoire saved = this.territoireRepository.save(territoire);
        return saved;
    }

    @Override
    public Territoire update(Territoire territoire, String code) {
        Territoire territoireFound = this.territoireRepository.findById(code).orElse(null);
        if (territoireFound == null){
            return null;
        }
        territoireFound.setLibelle(territoire.getLibelle());
        territoireFound.setTypeTerritoire(territoire.getTypeTerritoire());
        territoireFound.setTerritoire(territoire.getTerritoire());
        Territoire updated = this.territoireRepository.save(territoire);
        return updated;
    }
}
