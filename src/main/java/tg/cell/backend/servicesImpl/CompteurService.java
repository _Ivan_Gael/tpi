package tg.cell.backend.servicesImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Compteur;
import tg.cell.backend.repositories.CompteurRepository;

import javax.persistence.LockModeType;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Service
public class CompteurService {

    @Autowired
    private CompteurRepository compteurRepository;

    @Lock(LockModeType.WRITE)
    public String getNext(String t, String prefix) {
        String id;
        String table = t + "-" + prefix;
        Compteur c = find(table);
        if (c == null) {
            id = prefix + Calendar.getInstance().get(Calendar.YEAR) + "000001";
            create(new Compteur(table, id));
            return id;
        } else {
            String annee = c.getValeur().substring(prefix.length(), prefix.length() + 4);
            String currannee = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            if (!annee.equals(currannee)) {
                id = prefix + currannee + "000001";
            } else {
                int val = Integer.valueOf(c.getValeur().substring(prefix.length() + 4)) + 1;
                DecimalFormat f = new DecimalFormat("000000");
                id = prefix + currannee + f.format(val);
            }
            c.setValeur(id);
            create(c);
            System.out.println(id);
            return id;
        }
    }

    public Compteur find(String id) {
        return compteurRepository.findById(id).orElse(null);
    }

    public Compteur create(Compteur entity) {
        return	compteurRepository.save(entity);
    }



    public void remove(Compteur entity) {
        compteurRepository.delete(entity);
    }

    public String generateRequete(String table, String type, Date dateSaisine) {
        String numeroRG;
        String year = new SimpleDateFormat("yyyy").format(dateSaisine);
        Compteur c = find(table + "-" + year);
        if (c == null) {
            numeroRG = "001"+type+"/" + year ;
            create(new Compteur(table + "-" + year, numeroRG));
            return numeroRG;
        } else {
            int val = Integer.valueOf(c.getValeur().substring(0, 3)) + 1;
            DecimalFormat f = new DecimalFormat("000");
            numeroRG = f.format(val)+type + "/" + year;
            c.setValeur(numeroRG);
            create(c);
            return numeroRG;
        }
    }

    public String generateRG(String table, String codeRG, Date dateSaisine) {
        String numeroRG;
        String year = new SimpleDateFormat("yyyy").format(dateSaisine);
        Compteur c = find(table + "-" + year);
        if (c == null) {
            numeroRG = "000001/" + year + "/" + codeRG;
            create(new Compteur(table + "-" + year, numeroRG));
            return numeroRG;
        } else {
            int val = Integer.valueOf(c.getValeur().substring(0, 6)) + 1;
            DecimalFormat f = new DecimalFormat("000000");
            numeroRG = f.format(val) + "/" + year + "/" + codeRG;
            c.setValeur(numeroRG);
            create(c);
            return numeroRG;
        }
    }

    public String generateNumDec(String table, String codeRG, Date dateAudience) {
        String numeroDec;
        String year = new SimpleDateFormat("yyyy").format(dateAudience);
        Compteur c = find(table + "-" + year + "-" + codeRG);
        if (c == null) {
            // numeroDec = "0001/" + year + "/" + codeRG;
            numeroDec = "0001/" + year;
            create(new Compteur(table + "-" + year + "-" + codeRG, numeroDec));
            return numeroDec +" du " +  new SimpleDateFormat("dd/MM/yyyy").format(dateAudience);
        } else {
            int val = Integer.valueOf(c.getValeur().substring(0, 4)) + 1;
            DecimalFormat f = new DecimalFormat("0000");
            //numeroDec = f.format(val) + "/" + year + "/" + codeRG;
            numeroDec = f.format(val) + "/" + year;
            c.setValeur(numeroDec);
            create(c);
            return numeroDec +" du " +  new SimpleDateFormat("dd/MM/yyyy").format(dateAudience);
        }
    }

    public String generateIncrement(String table,  Date dateSaisine) {
        String numeroRG;
        String year = new SimpleDateFormat("yyyy").format(dateSaisine);
        Compteur c = find(table + "-" + year);
        if (c == null) {
            numeroRG = "000001/" + year ;
            create(new Compteur(table + "-" + year, numeroRG));
            return numeroRG;
        } else {
            int val = Integer.valueOf(c.getValeur().substring(0, 6)) + 1;
            DecimalFormat f = new DecimalFormat("000000");
            numeroRG = f.format(val) + "/" + year;
            c.setValeur(numeroRG);
            create(c);
            return numeroRG;
        }
    }


}
