package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Dossier;
import tg.cell.backend.repositories.DossierRepository;
import tg.cell.backend.services.DossierService;

import java.util.Date;
import java.util.List;

@Service
public class DossierServiceImpl implements DossierService {

    private final DossierRepository dossierRepository;

    public DossierServiceImpl(DossierRepository dossierRepository) {
        this.dossierRepository = dossierRepository;
    }

    @Override
    public List<Dossier> findAll() {
        List<Dossier> dossiers = this.dossierRepository.findAll();
        return dossiers;
    }

    @Override
    public Dossier findById(Long id) {
        Dossier dossier = this.dossierRepository.findById(id).orElse(null);
        return dossier;
    }

    @Override
    public Dossier save(Dossier dossier) {
        dossier.setDateDemande(new Date());
        dossier.setDateSaisine(null);
        Dossier saved = this.dossierRepository.save(dossier);
        return saved;
    }

    @Override
    public Dossier update(Dossier dossier, Long id) {
        Dossier dossierFound = this.dossierRepository.findById(id).orElse(null);
        if (dossierFound == null) {
            return null;
        }
        dossierFound.setDateSaisine(dossier.getDateSaisine());
        dossierFound.setAnnee(dossier.getAnnee());
        dossierFound.setTypeActe(dossier.getTypeActe());
        Dossier updated = this.dossierRepository.save(dossier);
        return updated;
    }
}
