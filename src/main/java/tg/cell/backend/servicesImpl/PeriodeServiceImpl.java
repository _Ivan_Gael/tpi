package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Periode;
import tg.cell.backend.repositories.PeriodeRepository;
import tg.cell.backend.services.PeriodeService;

import java.util.List;

@Service
public class PeriodeServiceImpl implements PeriodeService {

    private final PeriodeRepository periodeRepository;

    public PeriodeServiceImpl(PeriodeRepository periodeRepository) {
        this.periodeRepository = periodeRepository;
    }

    @Override
    public List<Periode> findAll() {
        List<Periode> periodes = this.periodeRepository.findAll();
        return periodes;
    }

    @Override
    public Periode findByCode(String code) {
        Periode periode = this.periodeRepository.findById(code).orElse(null);
        return periode;
    }

    @Override
    public Periode save(Periode periode) {
        Periode saved = this.periodeRepository.save(periode);
        return saved;
    }

    @Override
    public Periode update(Periode periode, String code) {
        Periode periodeFound = this.periodeRepository.findById(code).orElse(null);
        if (periodeFound == null) {
            return null;
        }
        periodeFound.setLibelle(periode.getLibelle());
        Periode updated = this.periodeRepository.save(periode);
        return updated;
    }
}
