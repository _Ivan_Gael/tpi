package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Evenement;
import tg.cell.backend.repositories.EvenementRepository;
import tg.cell.backend.services.EvenementService;

import java.util.List;

@Service
public class EvenementServiceImpl implements EvenementService {

    private final EvenementRepository evenementRepository;

    public EvenementServiceImpl(EvenementRepository evenementRepository) {
        this.evenementRepository = evenementRepository;
    }

    @Override
    public List<Evenement> findAll() {
        List<Evenement> evenements = this.evenementRepository.findAll();
        return evenements;
    }

    @Override
    public Evenement findById(Long id) {
        Evenement evenement = this.evenementRepository.findById(id).orElse(null);
        return evenement;
    }

    @Override
    public Evenement save(Evenement evenement) {
        Evenement saved = this.evenementRepository.save(evenement);
        return saved;
    }

    @Override
    public Evenement update(Evenement evenement, Long id) {
        Evenement evenementFound = this.evenementRepository.findById(id).orElse(null);
        if (evenementFound == null) {
            return null;
        }
        evenementFound.setLibelle(evenement.getLibelle());
        Evenement updated = this.evenementRepository.save(evenement);
        return updated;
    }
}
