package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Statut;
import tg.cell.backend.repositories.StatutRepository;
import tg.cell.backend.services.StatutService;

import java.util.List;

@Service
public class StatutServiceImpl implements StatutService {

    private final StatutRepository statutRepository;

    public StatutServiceImpl(StatutRepository statutRepository) {
        this.statutRepository = statutRepository;
    }

    @Override
    public List<Statut> findAll() {
        List<Statut> statuts = this.statutRepository.findAll();
        return statuts;
    }

    @Override
    public Statut findByCode(String code) {
        Statut statut = this.statutRepository.findById(code).orElse(null);
        return statut;
    }

    @Override
    public Statut save(Statut statut) {
        Statut saved = this.statutRepository.save(statut);
        return saved;
    }

    @Override
    public Statut update(Statut statut, String code) {
        Statut statutFound = this.statutRepository.findById(code).orElse(null);
        if (statutFound == null) {
            return null;
        }
        statutFound.setLibelle(statut.getLibelle());
        Statut updated = this.statutRepository.save(statut);
        return updated;
    }
}
