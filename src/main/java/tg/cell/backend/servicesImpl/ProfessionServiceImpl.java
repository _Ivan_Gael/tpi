package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.Profession;
import tg.cell.backend.repositories.ProfessionRepository;
import tg.cell.backend.services.ProfessionService;

import java.util.List;

@Service
public class ProfessionServiceImpl implements ProfessionService {

    private final ProfessionRepository professionRepository;

    public ProfessionServiceImpl(ProfessionRepository professionRepository) {
        this.professionRepository = professionRepository;
    }

    @Override
    public List<Profession> findAll() {
        return this.professionRepository.findAll();
    }

    @Override
    public Profession findById(Long id) {
        Profession profession = this.professionRepository.findById(id).orElse(null);
        return profession;
    }

    @Override
    public Profession save(Profession profession) {
        return this.professionRepository.save(profession);
    }

    @Override
    public Profession update(Profession profession, Long id) {
        Profession professionFound = this.professionRepository.findById(id).orElse(null);
        if(professionFound == null){
            return null;
        }
        professionFound.setLibelle(profession.getLibelle());
        return this.professionRepository.save(professionFound);
    }
}
