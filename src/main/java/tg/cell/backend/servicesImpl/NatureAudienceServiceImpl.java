package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.NatureAudience;
import tg.cell.backend.repositories.NatureAudienceRepository;
import tg.cell.backend.services.NatureAudienceService;

import java.util.List;

@Service
public class NatureAudienceServiceImpl implements NatureAudienceService {

    private final NatureAudienceRepository natureAudienceRepository;

    public NatureAudienceServiceImpl(NatureAudienceRepository natureAudienceRepository) {
        this.natureAudienceRepository = natureAudienceRepository;
    }

    @Override
    public List<NatureAudience> findAll() {
        List<NatureAudience> natureAudiences = this.natureAudienceRepository.findAll();
        return natureAudiences;
    }

    @Override
    public NatureAudience findByCode(String code) {
        NatureAudience natureAudience = this.natureAudienceRepository.findById(code).orElse(null);
        return natureAudience;
    }

    @Override
    public NatureAudience save(NatureAudience natureAudience) {
        NatureAudience saved = this.natureAudienceRepository.save(natureAudience);
        return saved;
    }

    @Override
    public NatureAudience update(NatureAudience natureAudience, String code) {
        NatureAudience natureAudienceFound = this.natureAudienceRepository.findById(code).orElse(null);
        if (natureAudienceFound == null) {
            return null;
        }
        natureAudienceFound.setLibelle(natureAudience.getLibelle());
        NatureAudience updated = this.natureAudienceRepository.save(natureAudience);
        return updated;
    }
}
