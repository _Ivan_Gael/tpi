package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypePersonne;
import tg.cell.backend.repositories.TypePersonneRepository;
import tg.cell.backend.services.TypePersonneService;

import java.util.List;

@Service
public class TypePersonneServiceImpl implements TypePersonneService {

    private final TypePersonneRepository typePersonneRepository;

    public TypePersonneServiceImpl(TypePersonneRepository typePersonneRepository) {
        this.typePersonneRepository = typePersonneRepository;
    }

    @Override
    public List<TypePersonne> findAll() {
        List<TypePersonne> typePersonnes = this.typePersonneRepository.findAll();
        return typePersonnes;
    }

    @Override
    public TypePersonne findByCode(String code) {
        TypePersonne typePersonne = this.typePersonneRepository.findById(code).orElse(null);
        return typePersonne;
    }

    @Override
    public TypePersonne save(TypePersonne typePersonne) {
        TypePersonne saved = this.typePersonneRepository.save(typePersonne);
        return saved;
    }

    @Override
    public TypePersonne update(TypePersonne typePersonne, String code) {
        TypePersonne typePersonneFound = this.typePersonneRepository.findById(code).orElse(null);
        if (typePersonneFound == null){
            return null;
        }
        typePersonne.setLibelle(typePersonne.getLibelle());
        TypePersonne updated = this.typePersonneRepository.save(typePersonne);
        return updated;
    }
}
