package tg.cell.backend.servicesImpl;

import org.springframework.stereotype.Service;
import tg.cell.backend.entities.TypeDocument;
import tg.cell.backend.repositories.TypeDocumentRepository;
import tg.cell.backend.services.TypeDocumentService;

import java.util.List;

@Service
public class TypeDocumentServiceImpl implements TypeDocumentService {

    private final TypeDocumentRepository typeDocumentRepository;

    public TypeDocumentServiceImpl(TypeDocumentRepository typeDocumentRepository) {
        this.typeDocumentRepository = typeDocumentRepository;
    }

    @Override
    public List<TypeDocument> findAll() {
        List<TypeDocument> typeDocuments = this.typeDocumentRepository.findAll();
        return typeDocuments;
    }

    @Override
    public TypeDocument findByCode(String code) {
        TypeDocument typeDocument = this.typeDocumentRepository.findById(code).orElse(null);
        return typeDocument;
    }

    @Override
    public TypeDocument save(TypeDocument typeDocument) {
        TypeDocument saved = this.typeDocumentRepository.save(typeDocument);
        return saved;
    }

    @Override
    public TypeDocument update(TypeDocument typeDocument, String code) {
        TypeDocument typeDocumentFound = this.typeDocumentRepository.findById(code).orElse(null);
        if (typeDocumentFound == null) {
            return null;
        }
        typeDocument.setLibelle(typeDocument.getLibelle());
        TypeDocument updated = this.typeDocumentRepository.save(typeDocument);
        return updated;
    }
}
