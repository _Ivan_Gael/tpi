package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.TypeTerritoire;
import tg.cell.backend.services.TypeTerritoireService;

@RestController
@RequestMapping("/type-territoires")
public class TypeTerritoireController {

    private final TypeTerritoireService typeTerritoireService;

    public TypeTerritoireController(TypeTerritoireService typeTerritoireService) {
        this.typeTerritoireService = typeTerritoireService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.typeTerritoireService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        TypeTerritoire typeTerritoire = this.typeTerritoireService.findByCode(code);
        if (typeTerritoire == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(typeTerritoire, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody TypeTerritoire typeTerritoire) {
        if (typeTerritoire == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypeTerritoire saved = this.typeTerritoireService.save(typeTerritoire);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody TypeTerritoire typeTerritoire, @PathVariable("code") String code) {
        if (typeTerritoire == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypeTerritoire updated = this.typeTerritoireService.update(typeTerritoire,code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
