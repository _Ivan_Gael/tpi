package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.Territoire;
import tg.cell.backend.services.TerritoireService;


@RestController
@RequestMapping("/territoires")
public class TerritoireController {

    private final TerritoireService territoireService;

    public TerritoireController(TerritoireService territoireService) {
        this.territoireService = territoireService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.territoireService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        Territoire territoire = this.territoireService.findByCode(code);
        if (territoire == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(territoire, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody Territoire territoire) {
        if (territoire == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Territoire saved = this.territoireService.save(territoire);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody Territoire territoire, @PathVariable("code") String code) {
        if (territoire == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Territoire updated = this.territoireService.update(territoire,code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
