package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.TypePersonne;
import tg.cell.backend.services.TypePersonneService;


@RestController
@RequestMapping("/type-personnes")
public class TypePersonneController {

    private final TypePersonneService typePersonneService;

    public TypePersonneController(TypePersonneService typePersonneService) {
        this.typePersonneService = typePersonneService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.typePersonneService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        TypePersonne typePersonne = this.typePersonneService.findByCode(code);
        if (typePersonne == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(typePersonne, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody TypePersonne typePersonne) {
        if (typePersonne == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypePersonne saved = this.typePersonneService.save(typePersonne);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody TypePersonne typePersonne, @PathVariable("code") String code) {
        if (typePersonne == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypePersonne updated = this.typePersonneService.update(typePersonne,code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
