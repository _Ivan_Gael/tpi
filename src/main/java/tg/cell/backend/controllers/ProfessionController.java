package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.Personne;
import tg.cell.backend.entities.Profession;
import tg.cell.backend.services.ProfessionService;
import tg.cell.backend.servicesImpl.FileModel;

@RestController
@RequestMapping("/professions")
public class ProfessionController {

    private final ProfessionService professionService;

    public ProfessionController(ProfessionService professionService) {
        this.professionService = professionService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.professionService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        Profession profession = this.professionService.findById(id);
        if (profession == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(profession, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody Profession profession) {
        if (profession == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Profession saved = this.professionService.save(profession);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }



    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody Profession profession, @PathVariable("id") Long id) {
        if (profession == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Profession updated = this.professionService.update(profession, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
