package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.ProgrammationAudience;
import tg.cell.backend.services.ProgrammationAudienceService;


@RestController
@RequestMapping("/programmation-audiences")
public class ProgrammationAudienceController {

    private final ProgrammationAudienceService programmationAudienceService;

    public ProgrammationAudienceController(ProgrammationAudienceService programmationAudienceService) {
        this.programmationAudienceService = programmationAudienceService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.programmationAudienceService.findAll(), HttpStatus.OK);
    }


    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        ProgrammationAudience programmationAudience = this.programmationAudienceService.findById(id);
        if (programmationAudience == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(programmationAudience, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody ProgrammationAudience programmationAudience) {
        if (programmationAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        ProgrammationAudience saved = this.programmationAudienceService.save(programmationAudience);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody ProgrammationAudience programmationAudience,
                                    @PathVariable("id") Long id) {
        if (programmationAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        ProgrammationAudience updated = this.programmationAudienceService.update(programmationAudience, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
