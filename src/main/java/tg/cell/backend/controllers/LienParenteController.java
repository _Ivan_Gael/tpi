package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.LienParent;
import tg.cell.backend.services.LienParenteService;


@RestController
@RequestMapping("/lien-parente")
public class LienParenteController {

    private final LienParenteService lienParenteService;

    public LienParenteController(LienParenteService lienParenteService) {
        this.lienParenteService = lienParenteService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.lienParenteService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        LienParent lienParent = this.lienParenteService.findById(id);
        if (lienParent == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(lienParent, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody LienParent lienParent) {
        if (lienParent == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        LienParent saved = this.lienParenteService.save(lienParent);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody LienParent lienParent, @PathVariable("id") Long id) {
        if (lienParent == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        LienParent updated = this.lienParenteService.update(lienParent, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
