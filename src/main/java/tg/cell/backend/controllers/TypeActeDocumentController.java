package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.TypeActe;
import tg.cell.backend.entities.TypeActeDocument;
import tg.cell.backend.services.TypeActeDocumentService;

import java.util.List;

@RestController
@RequestMapping("/type-acte-documents")
public class TypeActeDocumentController {

    private final TypeActeDocumentService typeActeDocumentService;

    public TypeActeDocumentController(TypeActeDocumentService typeActeDocumentService) {
        this.typeActeDocumentService = typeActeDocumentService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.typeActeDocumentService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        TypeActeDocument typeActeDocument = this.typeActeDocumentService.findById(id);
        if (typeActeDocument == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(typeActeDocument, HttpStatus.OK);
    }


    @PostMapping("/typeActe")
    public ResponseEntity<?> findByTypeActe(@RequestBody TypeActe typeActe) {
        List<TypeActeDocument> typeActeDocuments = this.typeActeDocumentService.findByTypeActe(typeActe);
        return new ResponseEntity<>(typeActeDocuments, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody TypeActeDocument typeActeDocument) {
        if (typeActeDocument == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypeActeDocument saved = this.typeActeDocumentService.save(typeActeDocument);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody TypeActeDocument typeActeDocument,
                                    @PathVariable("id") Long id) {
        if (typeActeDocument == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypeActeDocument updated = this.typeActeDocumentService.update(typeActeDocument, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
