package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.NatureAudience;
import tg.cell.backend.services.NatureAudienceService;


@RestController
@RequestMapping("/nature-audiences")
public class NatureAudienceController {

    private final NatureAudienceService natureAudienceService;

    public NatureAudienceController(NatureAudienceService natureAudienceService) {
        this.natureAudienceService = natureAudienceService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.natureAudienceService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        NatureAudience natureAudience = this.natureAudienceService.findByCode(code);
        if (natureAudience == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(natureAudience, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody NatureAudience natureAudience) {
        if (natureAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        NatureAudience saved = this.natureAudienceService.save(natureAudience);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }


    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody NatureAudience natureAudience, @PathVariable("code") String code) {
        if (natureAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        NatureAudience updated = this.natureAudienceService.update(natureAudience, code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
