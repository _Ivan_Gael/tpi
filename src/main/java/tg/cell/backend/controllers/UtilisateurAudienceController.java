package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.UtilisateurAudience;
import tg.cell.backend.services.UtilisateurAudienceService;


@RestController
@RequestMapping("/utilisateur-audiences")
public class UtilisateurAudienceController {

    private final UtilisateurAudienceService utilisateurAudienceService;

    public UtilisateurAudienceController(UtilisateurAudienceService utilisateurAudienceService) {
        this.utilisateurAudienceService = utilisateurAudienceService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.utilisateurAudienceService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        UtilisateurAudience utilisateurAudience = this.utilisateurAudienceService.findById(id);
        if (utilisateurAudience == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(utilisateurAudience, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody UtilisateurAudience utilisateurAudience) {
        if (utilisateurAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        UtilisateurAudience saved = this.utilisateurAudienceService.save(utilisateurAudience);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }


    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody UtilisateurAudience utilisateurAudience,
                                    @PathVariable("id") Long id) {
        if (utilisateurAudience == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        UtilisateurAudience updated = this.utilisateurAudienceService.update(utilisateurAudience, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
