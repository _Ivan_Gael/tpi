package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.Centre;
import tg.cell.backend.services.CentreService;


@RestController
@RequestMapping("/centres")
public class CentreController {

    private final CentreService centreService;

    public CentreController(CentreService centreService) {
        this.centreService = centreService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.centreService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        Centre centre = this.centreService.findById(id);
        if (centre == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(centre, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody Centre centre) {
        if (centre == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        } else {
            if(this.centreService.findByLibelle(centre.getLibelle()) == null){
                Centre saved = this.centreService.save(centre);
                return new ResponseEntity<>(saved, HttpStatus.CREATED);
            }
            return new ResponseEntity<>("Ce centre existe déja!", HttpStatus.BAD_REQUEST);
        }

    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody Centre centre, @PathVariable("id") Long id) {
        if (centre == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        else{
            if(this.centreService.findByLibelle(centre.getLibelle()) == null){
                Centre updated = this.centreService.update(centre, id);
                if (updated == null) {
                    return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
                }
                return new ResponseEntity<>(updated, HttpStatus.OK);
            }
            return new ResponseEntity<>("Ce centre existe déja!", HttpStatus.BAD_REQUEST);
        }

    }
}
