package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.TypePartie;
import tg.cell.backend.services.TypePartieService;


@RestController
@RequestMapping("/type-parties")
public class TypePartieController {

    private final TypePartieService typePartieService;

    public TypePartieController(TypePartieService typePartieService) {
        this.typePartieService = typePartieService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.typePartieService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        TypePartie typePartie = this.typePartieService.findByCode(code);
        if (typePartie == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(typePartie, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody TypePartie typePartie) {
        if (typePartie == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypePartie saved = this.typePartieService.save(typePartie);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody TypePartie typePartie, @PathVariable("code") String code) {
        if (typePartie == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        TypePartie updated = this.typePartieService.update(typePartie, code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
