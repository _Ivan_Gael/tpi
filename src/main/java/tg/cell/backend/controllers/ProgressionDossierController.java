package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tg.cell.backend.entities.ProgressionDossier;
import tg.cell.backend.services.ProgressionDossierService;


@RestController
@RequestMapping("/progression-dossiers")
public class ProgressionDossierController {

    private final ProgressionDossierService progressionDossierService;

    public ProgressionDossierController(ProgressionDossierService progressionDossierService) {
        this.progressionDossierService = progressionDossierService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.progressionDossierService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{code}")
    public ResponseEntity<?> findByCode(@PathVariable("code") String code) {
        ProgressionDossier progressionDossier = this.progressionDossierService.findByCode(code);
        if (progressionDossier == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(progressionDossier, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody ProgressionDossier progressionDossier) {
        if (progressionDossier == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        ProgressionDossier saved = this.progressionDossierService.save(progressionDossier);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{code}")
    public ResponseEntity<?> update( @RequestBody ProgressionDossier progressionDossier, @PathVariable("code") String code) {
        if (progressionDossier == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        ProgressionDossier updated = this.progressionDossierService.update(progressionDossier, code);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}


