package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Document;
import tg.cell.backend.services.DocumentService;

@RestController
@RequestMapping("/documents")
public class DocumentController {

    private final DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll(){
        return new ResponseEntity<>(this.documentService.findAll(), HttpStatus.OK);
    }

    /*@PostMapping("/")
    public ResponseEntity<?> save(@RequestParam("fichier") MultipartFile file){
        Document doc = this.documentService.save(file);
        return new ResponseEntity<>(doc, HttpStatus.OK);
    }*/
}
