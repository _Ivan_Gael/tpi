package tg.cell.backend.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import tg.cell.backend.entities.Personne;
import tg.cell.backend.entities.TypeDocument;
import tg.cell.backend.services.PersonneService;
import tg.cell.backend.servicesImpl.FileModel;


@RestController
@RequestMapping("/personnes")
public class PersonneController {

    private final PersonneService personneService;

    public PersonneController(PersonneService personneService) {
        this.personneService = personneService;
    }

    @GetMapping("/")
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(this.personneService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        Personne personne = this.personneService.findById(id);
        if (personne == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(personne, HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<?> save( @RequestBody Personne personne) {
        if (personne == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Personne saved = this.personneService.save(personne);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PostMapping("/allProcess")
    public ResponseEntity<?> saveAllProcess(@RequestParam("demandeur") String demandeur,
                                            @RequestParam("typeActe") String typeActe,
                                            @RequestParam("typeDocuments") String[] typeDocuments,
                                            @RequestParam("fichiers[]") MultipartFile[] fichiers) {
        if (demandeur == null  || typeActe == null || typeDocuments.length == 0 || fichiers.length == 0) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Boolean saved = this.personneService.saveAllProcess(demandeur, typeActe,typeDocuments, fichiers);
        return new ResponseEntity<>(saved, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update( @RequestBody Personne personne, @PathVariable("id") Long id) {
        if (personne == null) {
            return new ResponseEntity<>("Données requises manquantes!", HttpStatus.BAD_REQUEST);
        }
        Personne updated = this.personneService.update(personne, id);
        if (updated == null) {
            return new ResponseEntity<>("Aucun élément trouvé!", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }
}
