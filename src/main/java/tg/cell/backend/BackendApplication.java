package tg.cell.backend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import tg.cell.backend.entities.*;
import tg.cell.backend.repositories.*;

import java.util.Arrays;

@SpringBootApplication
@EnableEurekaClient
public class BackendApplication implements CommandLineRunner {

	@Autowired
	private TypeTerritoireRepository typeTerritoireRepository;

	@Autowired
	private TypeActeRepository typeActeRepository;

	@Autowired
	private TypeDocumentRepository typeDocumentRepository;

	@Autowired
	private ProfessionRepository professionRepository;

	@Autowired
	private NatureAudienceRepository natureAudienceRepository;

	@Autowired
	private StatutRepository statutRepository;

	@Autowired
	private TypePartieRepository typePartieRepository;

	@Autowired
	private EvenementRepository evenementRepository;

	@Autowired
	private PersonneRepository personneRepository;

	@Autowired
	private JourRepository jourRepository;

	@Autowired
	private PeriodeRepository periodeRepository;



	public static void main(String[] args) {
		SpringApplication.run(BackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try {
			//creation des types de territoires
			if(this.typeTerritoireRepository.findAll() == null || this.typeTerritoireRepository.findAll().isEmpty()){
				TypeTerritoire typeTerritoire1 = new TypeTerritoire();
				typeTerritoire1.setCode("REG");
				typeTerritoire1.setLibelle("REGION");

				TypeTerritoire typeTerritoire2 = new TypeTerritoire();
				typeTerritoire2.setCode("COM");
				typeTerritoire2.setLibelle("COMMUNE");

				TypeTerritoire typeTerritoire3 = new TypeTerritoire();
				typeTerritoire3.setCode("PREF");
				typeTerritoire3.setLibelle("PREFECTURE");

				TypeTerritoire typeTerritoire4 = new TypeTerritoire();
				typeTerritoire4.setCode("VIL");
				typeTerritoire4.setLibelle("VILLE");

				this.typeTerritoireRepository
						.saveAll(Arrays.asList(typeTerritoire1,typeTerritoire2,typeTerritoire3,typeTerritoire4));
			}
			if(this.typeActeRepository.findAll() == null || this.typeActeRepository.findAll().isEmpty()){
				TypeActe typeActe1 = new TypeActe();
				typeActe1.setCode("JUG-SUP");
				typeActe1.setLibelle("JUGEMENT SUPPLETIF");
				typeActe1.setFrais(30000L);

				TypeActe typeActe2 = new TypeActe();
				typeActe2.setCode("JUG-RECT");
				typeActe2.setLibelle("JUGEMENT RECTIFICATIF");
				typeActe2.setFrais(30000L);

				TypeActe typeActe3 = new TypeActe();
				typeActe3.setCode("H-PV");
				typeActe3.setLibelle("HOMOLOGATION DE PV");
				typeActe3.setFrais(30000L);

				TypeActe typeActe4 = new TypeActe();
				typeActe4.setCode("RECO-PAT");
				typeActe4.setLibelle("RECONNAISSANCE DE PATERNITE");
				typeActe4.setFrais(30000L);

				TypeActe typeActe5 = new TypeActe();
				typeActe5.setCode("ANNU-ACTE");
				typeActe5.setLibelle("ANNULATION D'ACTE");
				typeActe5.setFrais(30000L);

				this.typeActeRepository.saveAll(Arrays.asList(typeActe1,typeActe2,typeActe3,typeActe4,typeActe5));
			}
			if(this.typeDocumentRepository.findAll() == null || this.typeDocumentRepository.findAll().isEmpty()){
				TypeDocument typeDocument1 = new TypeDocument();
				typeDocument1.setCode("NAISS");
				typeDocument1.setLibelle("ORIGINALE DE L'ACTE DE NAISSANCE");

				TypeDocument typeDocument2 = new TypeDocument();
				typeDocument2.setCode("FDEM");
				typeDocument2.setLibelle("FORMULAIRE DE DEMANDE");

				TypeDocument typeDocument3 = new TypeDocument();
				typeDocument3.setCode("AC_ACTE");
				typeDocument3.setLibelle("ANCIEN ACTE A RECTIFIER");

				this.typeDocumentRepository.saveAll(Arrays.asList(typeDocument1,typeDocument2,typeDocument3));
			}
			if(this.professionRepository.findAll() == null || this.professionRepository.findAll().isEmpty()){
				Profession profession1 = new Profession();
				profession1.setLibelle("ETUDIANT(E)");

				Profession profession2 = new Profession();
				profession2.setLibelle("AGENT DE SECURITE");

				Profession profession3 = new Profession();
				profession3.setLibelle("APPRENTI");

				Profession profession4 = new Profession();
				profession4.setLibelle("ENSEIGNANT");

				Profession profession5 = new Profession();
				profession5.setLibelle("COMPTABLE");

				Profession profession6 = new Profession();
				profession6.setLibelle("CUISINIER(E)");

				Profession profession7 = new Profession();
				profession7.setLibelle("CONSEILLER COMMERCIAL");

				Profession profession8 = new Profession();
				profession8.setLibelle("ENTREPRENEUR");

				Profession profession9 = new Profession();
				profession9.setLibelle("GEOMETRE");

				Profession profession10 = new Profession();
				profession10.setLibelle("MEDECIN");

				Profession profession11 = new Profession();
				profession11.setLibelle("PHYSICIEN");

				Profession profession12 = new Profession();
				profession12.setLibelle("MATHEMATICIEN");

				Profession profession13 = new Profession();
				profession13.setLibelle("PHARMACIEN");

				Profession profession14 = new Profession();
				profession14.setLibelle("DEVELOPPEUR d'APPLICATION");

				Profession profession15 = new Profession();
				profession15.setLibelle("ADMINISTRATEUR DE BASE DE DONNEES");

				Profession profession16 = new Profession();
				profession10.setLibelle("AUTRE");

				this.professionRepository.
						saveAll(Arrays.asList
								(profession1,profession2,
										profession3,
										profession4,profession5,profession6,profession7,profession8,
										profession9,profession10,profession11,profession12,profession13,
										profession14,profession15,profession16));

			}
			if(this.statutRepository.findAll() == null || this.statutRepository.findAll().isEmpty()){
				Statut statut1 = new Statut();
				statut1.setCode("NEW");
				statut1.setLibelle("NOUVEAU");

				Statut statut2 = new Statut();
				statut2.setCode("AUD");
				statut2.setLibelle("AUDIENCE");

				Statut statut3 = new Statut();
				statut3.setCode("RAD");
				statut3.setLibelle("RENVOI");

				Statut statut4 = new Statut();
				statut4.setCode("DEL");
				statut4.setLibelle("MISE EN DELIBERE");

				Statut statut5 = new Statut();
				statut5.setCode("END");
				statut5.setLibelle("VIDE");

				Statut statut6 = new Statut();
				statut6.setCode("RDI");
				statut6.setLibelle("RADIE");

				this.statutRepository.saveAll(Arrays.asList(statut1,statut2,statut3,statut4,statut5,statut6));

			}
			if(this.natureAudienceRepository.findAll() == null || this.natureAudienceRepository.findAll().isEmpty()){
				NatureAudience natureAudience1 = new NatureAudience();
				natureAudience1.setCode("ORD");
				natureAudience1.setLibelle("ORDINAIRE");

				NatureAudience natureAudience2 = new NatureAudience();
				natureAudience2.setCode("HOMOLO");
				natureAudience2.setLibelle("HOMOLOGATION");

				this.natureAudienceRepository.saveAll(Arrays.asList(natureAudience1,natureAudience2));
			}
			if(this.typePartieRepository.findAll() == null || this.typePartieRepository.findAll().isEmpty()){
				TypePartie typePartie1 = new TypePartie();
				typePartie1.setCode("DEM");
				typePartie1.setLibelle("DEMANDEUR");

				TypePartie typePartie2 = new TypePartie();
				typePartie2.setCode("DEF");
				typePartie2.setLibelle("DEFENDEUR");

				TypePartie typePartie3 = new TypePartie();
				typePartie3.setCode("AVO");
				typePartie3.setLibelle("AVOCAT");

				this.typePartieRepository.saveAll(Arrays.asList(typePartie1,typePartie2,typePartie3));
			}
			if(this.evenementRepository.findAll() == null || this.evenementRepository.findAll().isEmpty()){
				Evenement evenement1 = new Evenement();
				evenement1.setLibelle("CREATION DE DOSSIER");

				Evenement evenement2 = new Evenement();
				evenement2.setLibelle("MISE EN DELIBERE");

				Evenement evenement3 = new Evenement();
				evenement3.setLibelle("AUDIENCE");

				Evenement evenement4 = new Evenement();
				evenement4.setLibelle("RENVOI");

				Evenement evenement5 = new Evenement();
				evenement5.setLibelle("DECISION");

				Evenement evenement6 = new Evenement();
				evenement6.setLibelle("ACTE IMPRIME");

				this.evenementRepository.saveAll(Arrays.asList(evenement1,
						evenement2,evenement3,evenement4,evenement5,evenement6));
			}
			if(this.jourRepository.findAll() == null || this.jourRepository.findAll().isEmpty()){
				Jour jour1 = new Jour();
				jour1.setCode("DIMANCHE");
				jour1.setLibelle("Dimanche");

				Jour jour2 = new Jour();
				jour2.setCode("LUNDI");
				jour2.setLibelle("Lundi");

				Jour jour3 = new Jour();
				jour3.setCode("MARDI");
				jour3.setLibelle("Mardi");

				Jour jour4 = new Jour();
				jour4.setCode("MERCREDI");
				jour4.setLibelle("Mercredi");

				Jour jour5 = new Jour();
				jour5.setCode("JEUDI");
				jour5.setLibelle("Jeudi");

				Jour jour6 = new Jour();
				jour6.setCode("VENDREDI");
				jour6.setLibelle("Vendredi");

				Jour jour7 = new Jour();
				jour7.setCode("SAMEDI");
				jour7.setLibelle("Samedi");

				this.jourRepository.saveAll(Arrays.asList(jour1,jour2,jour3,jour4,jour5,jour6,jour7));
			}
			if(this.periodeRepository.findAll() == null || this.periodeRepository.findAll().isEmpty()){
				Periode periode1 = new Periode();
				periode1.setCode("HEBDOMADAIRE");
				periode1.setLibelle("Hebdomadaire");

				Periode periode2 = new Periode();
				periode2.setCode("QUINZAINE");
				periode2.setLibelle("Quinzaine");

				Periode periode3 = new Periode();
				periode3.setCode("TRIMESTRIELDEBUT");
				periode3.setLibelle("Début trimestre");

				Periode periode4 = new Periode();
				periode4.setCode("TRIMESTRIELFIN");
				periode4.setLibelle("Fin trimestre");

				Periode periode5 = new Periode();
				periode5.setCode("MENSUELDEBUT");
				periode5.setLibelle("Premier du mois");

				Periode periode6 = new Periode();
				periode6.setCode("MENSUELFIN");
				periode6.setLibelle("Dernier du mois");

				Periode periode7 = new Periode();
				periode7.setCode("MENSUELDEUXIEMESEMAINE");
				periode7.setLibelle("Deuxieme semaine du mois");

				Periode periode8 = new Periode();
				periode8.setCode("MENSUELTROISIEMESEMAINE");
				periode8.setLibelle("Troisieme semaine du mois");

				this.periodeRepository.saveAll(Arrays.asList(periode1,periode2,periode3,periode4,periode5,
						periode6,periode7,periode8));
			}

		} catch (Exception e){
			e.printStackTrace();
		}

	}
}
